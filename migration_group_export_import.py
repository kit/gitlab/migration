#!/usr/bin/env python3
"""\
Exprot and Import a group from source(pilot) GitLab to new GitLab

Usage: migration_group_export_import.py

Author: Donghee Kang<donghee.kang@kit.edu>
        Felix Dörre<felix.doerre@kit.edu>

"""
import gitlab
import os
import sys
import urllib3
import json
import time
from tqdm import tqdm

##########################################################################################################
# Credential
##########################################################################################################
KIT_PILOT_URL = "https://git.scc.kit.edu"
KIT_GITLAB_URL = "https://gitlab.kit.edu"

KIT_PILOT_TOKEN = "---------------------------------"   # Personal Access Token, please change with yours
KIT_GITLAB_TOKEN = "----------------------------------" # Personal Access Token, please change with yours

gl_kit = gitlab.Gitlab(KIT_GITLAB_URL, private_token=KIT_GITLAB_TOKEN, ssl_verify=True, api_version="4")
gl_pilot = gitlab.Gitlab(KIT_PILOT_URL, private_token=KIT_PILOT_TOKEN, ssl_verify=True, api_version="4")

##########################################################################################################
# Parameter
##########################################################################################################
# source group id
group_id_in_source = 19277          # required parameter

# In target gitlab, group path has to define. "KIT / Institute" must be existing.
# If the last subgroup "Migration" is not existing, 
#          "Migration" will be created first and projects wil be transferred on this subgroup. 
# If the last subgroup "Migration" is already existing, 
#          then projects will be transferred to existing subgroup, directly. 
group_namespace_in_target = 'kit/institute/migration' 

##########################################################################################################
# Migration
##########################################################################################################
class migration():
    def __init__(self):
        gl_pilot.auth()
        gl_kit.auth()


    def get_root_group(self):
        # create last subgroup if it is not existing
        structure_of_group = group_namespace_in_target.split('/')
        subgroup_id = None
        layer = 0
        subgroup = None
        for group_name in structure_of_group[:-1]:
            layer += 1

            if layer == 1:
                group = gl_kit.groups.get(group_name.upper())
            else:
                group = gl_kit.groups.get(subgroup_id)

            parent_group_id = subgroup_id
            subgroup = self.get_subgroup(group, structure_of_group[layer])
            if subgroup is not None:
                subgroup_id = subgroup.id

            # create last subgroup, and provide its id
            if (layer == (len(structure_of_group)-1)) and (subgroup is None):
                print("not found")
                exit()
                new_subgroup = gl_kit.groups.create(
                    {
                        'name': structure_of_group[layer].capitalize(),
                        'path': structure_of_group[layer],
                        'parent_id': parent_group_id
                    }
                )
                new_subgroup.save()
                time.sleep(3)
                subgroup_id = new_subgroup.id
                print("New subgroup is created on the path : {}".format(group_namespace_in_target))

        return subgroup

    def get_subgroup(self, group, path):
        for subgroup in group.subgroups.list(get_all=True, iterator=True):
                if subgroup.attributes.get('path') == path:
                    return gl_kit.groups.get(subgroup.id)
        return None

    def migrate_projects(self):
        # To perform a new export, a group id must to be given.
        if group_id_in_source is None:
            print("Group ID is not provided, please find the group id from the source gitlab")
            exit()

        self.migrate(gl_pilot.groups.get(group_id_in_source, lazy=False), self.get_root_group())

    def wait_until_finished(self, project_export, project_name, field="export_status", process = "Exporting"):
        project_export.refresh()
        with tqdm(total=100, desc="{} {}".format(process, project_name), bar_format="{l_bar}{bar} [ elapsed: {elapsed} ]") as pbar:
            while getattr(project_export, field) != 'finished':
                time.sleep(3)
                project_export.refresh()
                pbar.total += 50
                pbar.update(50)
            pbar.update(100)

    def migrate(self, group, group_in_target, local_path="", indent_prefix="", recurse=True):
        current_working_directory = os.getcwd()
        exported_file_directory = current_working_directory + "/exported_files" + local_path
        os.makedirs(exported_file_directory, exist_ok=True)

        print(indent_prefix + "* " + group.name)
        print(indent_prefix + "from: " + group.web_url)
        print(indent_prefix + "to:   " + group_in_target.web_url)
        if(group_in_target.project_creation_level == "noone"):
            print("Activating project creation")
            group_in_target.project_creation_level = "maintainer"
            group_in_target.save()
        indent_prefix += "  "
        if(recurse):
            for subgroup in group.subgroups.list(get_all=True, iterator=True):
                self.migrate(gl_pilot.groups.get(subgroup.id, lazy=False),  self.get_subgroup(group_in_target, subgroup.path), local_path + "/" + subgroup.path, indent_prefix, recurse)

        projects = group.projects.list(get_all=True, iterator=True)
        existing = [project.path for project in group_in_target.projects.list(get_all=True,iterator=True)]
        for project in (project for project in projects if len(projects) > 0):
            if project.path in existing:
                print(indent_prefix + "* Project {} already exists in target, skipping.".format(project.name))
                continue

            project_name = project.name.replace(' ', '-')

            # Get the project object using project_id, project itself does not have exports function.
            single_project = gl_pilot.projects.get(project.id)
            project_export = single_project.exports.create()
            # Wait for the 'finished' status of export, but python-gitlab version dependent!
            print(indent_prefix + "* Migrating project {} from the source GitLab.".format(project_name))
            self.wait_until_finished(project_export, project_name)

            # Download the result
            file_is = '{}/{}_export.tar.gz'.format(exported_file_directory, project_name)
            with open(file_is, 'wb') as f:
                project_export.download(streamed=True, action=f.write)

            self.import_projects(project, file_is, group_in_target, project_name)


    def import_projects(self, source_project, filename, target_group, project_name):
        with open(filename, 'rb') as f:
            output = gl_kit.projects.import_project(
                f,
                path=source_project.path,
                name=source_project.name,
                namespace=target_group.id,
                override_params={'visibility': 'private'},
            )

        # Get a ProjectImport object to track the import status
        project_import = gl_kit.projects.get(output['id'], lazy=True).imports.get()
        self.wait_until_finished(project_import, project_name, "import_status", "Importing")

if __name__ == "__main__":

    # let's start
    migration_accessor = migration()
    migration_accessor.migrate_projects()
