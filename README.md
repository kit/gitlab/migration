# Migration of project and group

Useful python migration tools

Author: Donghee Kang, donghee.kang@kit.edu. 
        Felix Dörre, felix.doerre@kit.edu

&nbsp;

## Requirement

`python-gitlab` requires `Python 3.8+` at least.

Use pip to install the latest stable version of python-gitlab
> pip install --upgrade python-gitlab 
> pip install tqdm

&nbsp;


## 1. **Group** from pilot gitlab to new GitLab at KIT

To export groups recursively and import them one-by-one under correct name and path, this script helps users who want to migrate their entire group structure.

<img src="./fig/Migration_structure.png" width="1000" height="500">

Usage: migration_group_export_import.py

1. add your personal access token (PAT) getting from the source and target gitlab both. 
2. In source(pilot) gitlab, find out the group id, which will be migrated. You should access it from the web browser. 
3. In target(new) gitlab, define the `path`` of your subgroup. (not group name)
    - `group_namespace_in_target = 'kit/institute/my_group'`

In the target gitlab, a group structure "KIT / Institute" has to be eanbled in advance.
If the last subgroup "my_group" is not existing, "my_group" will be created first and projects wil be transferred on this subgroup. 
If the last subgroup "my_group" is already existing, then all projects will be transferred to existing subgroup, directly. 

> $ python3 migration_group_export_import.py

Procedure: 
1. Exporting projects in a group `my_group` from the source gitlab. You needs to know `group_id` for this group `my_group`
2. Download exported files to the local directory `"{PWD}/exported_files"` 
3. Importing the exported files to the target gitlab on `"kit/institute/my_group"`

> **notice** OE group or a private group must be existing in the second layer (instistitut level = OE group level)

&nbsp;

## 2. Exported project files transfer to the Group on new GitLab at KIT
Usage: migration_group_import_only.py
1. add your personal access token (PAT) getting from the target gitlab only. 
2. define a set of exported files, that are located under the local directory `"{PWD}/exported_files"`
3. In target(new) gitlab, define the path of your subgroup
    - `group_namespace_in_target = 'kit/institute/my_group'`

In the target gitlab, a group structure "KIT / Institute" has to be eanbled in advance.
If the last subgroup "my_group" is not existing, "my_group" will be created first and projects wil be transferred on this subgroup. 
If the last subgroup "my_group" is already existing, then all projects will be transferred to existing subgroup, directly. 

> $ python3 migration_group_improt_only.py

Procedure: 
1. No exporting and download is requried. In the directory`"{PWD}/exported_files"` all `*_exported.tar.gz"` files(=projects) are enabled to use.   
2. Importing the exported files to the target gitlab on the subgroup `"kit/institute/my_group"`


&nbsp;

## 3. **Project** from pilot gitlab to new GitLab at KIT
Usage: migration_project_export_improt.py

1. add your personal access token (PAT) getting from the source and target gitlab both. 
2. In source(pilot) gitlab, find the project id or project namespace.
    - `project_id_in_source = 123456789`          
    - `source_namespace = 'xx1234/my_project'` 
3. In target(new) gitlab, define the private namespace. It would be <vornaem.nachname> in general. 
    - `target_namespace = 'firstname.lastname'`

If project_id_in_source is used, you do not need to defind source_namespace. But if you do not use project_id_in_source, then you have to write the project namespace(path) explicitly.
For the target namespace, you have to define your private namespace as default, where the project will be transferred. 

> $ python3 migration_group_export_import.py

Procedure:
1. Exporting a single project from the source gitlab. 
2. Download exported file to the local directory `"{PWD}/exported_files"`
3. Importing the exported file to the target gitlab on private namesapce `"firstname.lastname"`

